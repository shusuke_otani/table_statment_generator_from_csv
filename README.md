## table_stmt_generator_from_csv
generate the sql statement to convert csv to table.

## Usage
usage: execute.py [-h] [-d {m,p}] [-t] [-p PK] csv_file_path

positional arguments:
  csv_file_path         set the csv file path

optional arguments:
  -h, --help            show this help message and exit
  -d {m,p}, --database_type {m,p}
                        select database type. m means mysql, p means postgre
  -t, --is_temp         is temporary table?
  -p PK, --pk PK        set pk(use commma if composite primary key)

## Example
python execute.py -d m -t -p id  ./example/test.csv

## Installation
0. install pip
1. git clone and cd
2. pip install -r requirements.txt
3. python execute.py /path/to/your/csv

## Requirements
python2.7.1
