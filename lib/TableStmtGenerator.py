#coding: UTF-8
from typeguesser import TypeGuesser
from abc import ABCMeta, abstractmethod
import re
import os

class AbstractTableStmtGenerator(object):
    __metaclass__ = ABCMeta
    def __init__(self, file_path, is_temp, pk):
        self.file_path = os.path.abspath(file_path)
        self.table_name = re.search(r"(\w+).csv", self.file_path).group(1)
        self.is_temp = is_temp
        self.pk = pk

    def generate(self):
        table_create_statement = self.generate_table_create_statement()
        copy_create_statement = self.generate_copy_stmt()

        return 'DROP TABLE IF EXISTS ' + self.table_name + '; ' + table_create_statement + copy_create_statement

    def generate_table_create_statement(self):
        tg = TypeGuesser(self.file_path, header=True, tableName=self.table_name)
        tg.guessTypes()

        create_stmt = tg.getCreateStatement()
        if self.is_temp: create_stmt = create_stmt.replace('CREATE TABLE', 'CREATE TEMPORARY TABLE')
        if self.pk: create_stmt = create_stmt.replace(');', ', PRIMARY KEY(' + self.pk + '));' )
        return create_stmt.replace('boolean', 'smallint')

    @abstractmethod
    def generate_copy_stmt(self):
        pass

class MysqlTableStmtGenerator(AbstractTableStmtGenerator):
    def generate_copy_stmt(self):
        copy_stmt = 'LOAD DATA LOCAL INFILE ' + "'" + self.file_path + "' INTO TABLE " + self.table_name + " FIELDS TERMINATED BY ','  IGNORE 1 LINES;"

        return copy_stmt

class PostgreTableStmtGenerator(AbstractTableStmtGenerator):
    def generate_copy_stmt(self):
        copy_stmt = 'COPY ' + self.table_name + ' FROM ' + "'" + self.file_path + "'" + ' CSV HEADER;'

        return copy_stmt
