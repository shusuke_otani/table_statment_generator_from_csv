#!/usr/bin/python
# coding: utf-8
import argparse
from lib.TableStmtGenerator import *

parser = argparse.ArgumentParser()
parser.add_argument('csv_file_path', type=str, help='set the csv file path')
parser.add_argument('-d', '--database_type', choices=['m', 'p'], help='select database type. m means mysql, p means postgre')
parser.add_argument('-t', '--is_temp', help='is temporary table?', action='store_true')
parser.add_argument('-p', '--pk', type=str, help='set pk(use commma if composite primary key)')
argv_args = parser.parse_args()

table_creator_args = {'file_path': argv_args.csv_file_path}
if argv_args.is_temp: table_creator_args['is_temp'] = True
if argv_args.is_temp: table_creator_args['pk'] = argv_args.pk

if argv_args.database_type == 'm' : table_stmt_generator =  MysqlTableStmtGenerator(**table_creator_args)
if argv_args.database_type == 'p' : table_stmt_generator =  PostgreTableStmtGenerator(**table_creator_args)

print(table_stmt_generator.generate())
